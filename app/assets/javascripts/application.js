// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery3
//= require_tree .

$(document).on('turbolinks:load', function() {
    $inputImages = $('#imagesInput');
    $previewBlock = $('#previews');
});

function showPreview() {
    clearPreview();
    for (let i = 0; i < $inputImages[0].files.length; i++) {
        addPreview($inputImages[0].files[i], i);
    }
}

function clearPreview() {
    $previewBlock.empty();
}

function addPreview(file, index) {
    var reader = new FileReader();
    var img = document.createElement("img");
    img.classList.add('image-preview');
    $previewBlock.append(img);
    reader.onloadend = function () {
        img.src = reader.result;
    };
    reader.readAsDataURL(file);
}