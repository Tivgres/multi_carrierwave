class Project < ApplicationRecord
  mount_uploaders :images, ImageUploader

  validates :title, :description, presence: true
end
